-- Plugins
require('packer').startup(function(use)
    use 'wbthomason/packer.nvim'
    use 'neovim/nvim-lspconfig'
    use 'lukas-reineke/lsp-format.nvim'
    use 'nvim-lua/plenary.nvim'
    use 'nvim-telescope/telescope.nvim'
    use 'nvim-treesitter/nvim-treesitter'
    use 'folke/tokyonight.nvim'
end)

-- Most important keymaps
vim.keymap.set('n', ';', ':')
vim.keymap.set('i', 'jk', '<esc>')

-- Terminal
vim.keymap.set('t', 'jk', '<c-\\><c-n>')
vim.opt.shell = 'fish'

-- Clipboard support
vim.opt.clipboard = 'unnamedplus'

-- Faster navigation keymaps
vim.keymap.set('n', 'J', '30j')
vim.keymap.set('n', 'K', '30k')

-- Misc editing config
vim.opt.number = true
vim.opt.cursorline = true
vim.opt.colorcolumn = "80"
vim.wo.wrap = false

-- Quickfix keymaps
vim.keymap.set('n', '<C-c>', ':cclose<cr>')
vim.keymap.set('n', '<C-j>', ':cnext<cr>')
vim.keymap.set('n', '<C-k>', ':cprev<cr>')

-- Tabs config
vim.opt.tabstop = 4
vim.opt.softtabstop = 4
vim.opt.shiftwidth = 4
vim.opt.expandtab = true

-- Colors
vim.cmd [[ colorscheme tokyonight-night ]]

-- Completion
vim.o.completeopt = "menuone,noinsert,noselect"

-- Generic LSP config
require('lsp-format').setup {}
local on_attach = function(client, bufnr)
    -- Format
    require('lsp-format').on_attach(client, bufnr)

    -- Completion
    vim.api.nvim_buf_set_option(bufnr, 'omnifunc', 'v:lua.vim.lsp.omnifunc')

    -- Bindings
    local opts = { noremap = true, silent = true, buffer = bufnr }

    vim.keymap.set('n', 'gd', vim.lsp.buf.definition, ops)
    vim.keymap.set('n', 'gx', vim.lsp.buf.references, ops)
    vim.keymap.set('n', 'L', vim.lsp.buf.hover, ops)
    vim.keymap.set('n', '}', vim.diagnostic.goto_next, ops)
    vim.keymap.set('n', '{', vim.diagnostic.goto_prev, ops)
    vim.keymap.set('n', 'ga', vim.lsp.buf.code_action, ops)
end

 -- LSP servers
require('lspconfig').pyright.setup { on_attach = on_attach }
require('lspconfig').pylsp.setup {
    on_attach = on_attach,
    settings = {
        pylsp = {
            plugins = {
                flake8 = { enabled = true, ignore={"E501", "W503"}},
                isort = { enabled = true, profile = "black" },
                pyls_black = { enabled = true },
                pycodestyle = {
                    enabled = false
                },
                pylsp_mypy = { enabled = true },
            }
        }
    }
}

-- Telescope
local telescope = require('telescope.builtin')
vim.keymap.set('n', '<c-p>', telescope.find_files)
vim.keymap.set('n', '<c-a><c-f>', telescope.live_grep)
vim.keymap.set('n', '<c-b>', telescope.buffers)
vim.keymap.set('n', '<c-f>', telescope.current_buffer_fuzzy_find)

-- Treesitter
require('nvim-treesitter.configs').setup {
    ensure_installed = { "c", "lua", "vim", "vimdoc", "query" },

    highlight = {
        enable = true,
        additional_vim_regex_highlighting = false
    }
}

-- Gate
vim.filetype.add({
    extension = {
        gate = 'gate'
    }
})

local parser_config = require('nvim-treesitter.parsers').get_parser_configs()
parser_config.gate = {
    install_info = {
        url = "~/work/hexacore/gate/compile/tree-sitter-gate",
        files = {"src/parser.c"}
    },
    filetype = "gate"
}
