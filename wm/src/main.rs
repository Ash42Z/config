use penrose::{
    builtin::{
        actions::{exit, log_current_state, modify_with, send_layout_message,spawn},
        layout::{
            messages::{ExpandMain, IncMain, ShrinkMain},
            transformers::{Gaps, ReserveTop},
            MainAndStack, Monocle,
        },
    },
    core::{
        bindings::{parse_keybindings_with_xmodmap, KeyEventHandler},
        layout::LayoutStack,
        Config, WindowManager,
    },
    extensions::hooks::add_ewmh_hooks,
    map, stack,
    x11rb::RustConn,
    Result,
};

use penrose_ui::{
    bar::{StatusBar, Position},
    bar::widgets::{
        ActiveWindowName,
        CurrentLayout,
        Workspaces,
        battery_summary,
        current_date_and_time,
    },
    core::TextStyle
};
use std::collections::HashMap;
use tracing_subscriber::{self, prelude::*};

const FONT: &str = "Noto Sans";

const BLACK: u32 = 0x15161eff;
const WHITE: u32 = 0xa9b1d6ff;
const GREY: u32 = 0x1a1b26ff;

const MAX_MAIN: u32 = 1;
const RATIO: f32 = 0.6;
const RATIO_STEP: f32 = 0.1;
const OUTER_PX: u32 = 5;
const INNER_PX: u32 = 5;
const BAR_HEIGHT_PX: u32 = 48;

const MODIFIER: &str = "A";

fn modifier_and(keys: &str) -> String {
    format!("{}-{}", MODIFIER, keys)
}

fn raw_key_bindings() -> HashMap<String, Box<dyn KeyEventHandler<RustConn>>> {
    let mut raw_bindings = map! {
        modifier_and("j") => modify_with(|cs| cs.focus_down()),
        modifier_and("k") => modify_with(|cs| cs.focus_up()),

        modifier_and("S-j") => modify_with(|cs| cs.swap_down()),
        modifier_and("S-k") => modify_with(|cs| cs.swap_up()),

        modifier_and("q") => modify_with(|cs| cs.kill_focused()),

        modifier_and("Tab") => modify_with(|cs| cs.toggle_tag()),

        modifier_and("bracketright") => modify_with(|cs| cs.next_screen()),
        modifier_and("bracketleft") => modify_with(|cs| cs.previous_screen()),

        modifier_and("grave") => modify_with(|cs| cs.next_layout()),
        modifier_and("S-grave") => modify_with(|cs| cs.previous_layout()),

        modifier_and("Up") => send_layout_message(|| IncMain(1)),
        modifier_and("Down") => send_layout_message(|| IncMain(-1)),
        modifier_and("Right") => send_layout_message(|| ExpandMain),
        modifier_and("Left") => send_layout_message(|| ShrinkMain),

        modifier_and("S-s") => log_current_state(),

        modifier_and("o") => spawn("rofi -show run"),
        modifier_and("r") => spawn("~/scripts/scr"),

        modifier_and("Return") => spawn("alacritty"),
        modifier_and("i") => spawn("chromium"),
        modifier_and("S-Escape") => exit(),
    };

    for tag in &["1", "2", "3", "4", "5", "6", "7", "8", "9"] {
        raw_bindings.extend([
            (modifier_and(tag), modify_with(move |cs| cs.focus_tag(tag))),
            (modifier_and(format!("S-{}", tag).as_str()), modify_with(move |cs| cs.move_focused_to_tag(tag))),
        ]);
    }

    raw_bindings
}

fn layouts() -> LayoutStack {
    stack!(
        MainAndStack::side(MAX_MAIN, RATIO, RATIO_STEP),
        MainAndStack::side_mirrored(MAX_MAIN, RATIO, RATIO_STEP),
        MainAndStack::bottom(MAX_MAIN, RATIO, RATIO_STEP),
        Monocle::boxed()
    )
    .map(|layout| ReserveTop::wrap(Gaps::wrap(layout, OUTER_PX, INNER_PX), BAR_HEIGHT_PX))
}

fn main() -> Result<()> {
    tracing_subscriber::fmt()
        .with_env_filter("debug")
        .finish()
        .init();

    let config = add_ewmh_hooks(Config {
        default_layouts: layouts(),
        focused_border: GREY.into(),
        ..Config::default()
    });

    let conn = RustConn::new()?;
    let bindings = parse_keybindings_with_xmodmap(raw_key_bindings())?;
    let style = TextStyle {
        fg: WHITE.into(),
        bg: Some(BLACK.into()),
        padding: (2, 2),
    };

    let bar = StatusBar::try_new(
        Position::Top,
        BAR_HEIGHT_PX,
        BLACK,
        FONT,
        12,
        vec![
            Box::new(Workspaces::new(style, WHITE, GREY)),
            Box::new(CurrentLayout::new(style)),
            Box::new(ActiveWindowName::new(
                80,
                TextStyle {
                    bg: Some(GREY.into()),
                    padding: (6, 4),
                    ..style
                },
                true,
                false,
            )),
            Box::new(current_date_and_time(style)),
            Box::new(battery_summary("BAT0", style))
        ]
    ).unwrap();

    let wm = bar.add_to(WindowManager::new(
        config,
        bindings,
        HashMap::new(),
        conn,
    )?);

    wm.run()
}
